//! Provides a type representing a Redis protocol frame as well as utilities for
//! parsing frames from a byte array.

use std::convert::TryInto;
use std::fmt;
use std::io::Cursor;
use std::num::TryFromIntError;
use std::string::FromUtf8Error;

use bytes::{Buf, Bytes};

/// A frame in the Redis protocol.
#[derive(Clone, Debug)]
pub enum Frame {
    Simple(String),
    Error(String),
    Integer(u64),
    Bulk(Bytes),
    Null,
    Array(Vec<Frame>),
}

#[derive(Debug)]
pub enum Error {
    /// Not enough data is available to parse a message
    Incomplete,

    /// Invalid message encoding
    Other(crate::Error),
}

impl Frame {
    /// Returns an empty array
    pub(crate) fn array() -> Frame {
        Frame::Array(vec![])
    }

    /// Push a "bulk" frame into the array. `self` must be an Array frame.
    ///
    /// # Panics
    ///
    /// panics if `self` is not an array
    // 将一个 Bulk 帧放入 Array 帧中
    pub(crate) fn push_bulk(&mut self, bytes: Bytes) {
        match self {
            Frame::Array(vec) => {
                vec.push(Frame::Bulk(bytes));
            }
            _ => panic!("not an array frame"),
        }
    }

    /// Push an "integer" frame into the array. `self` must be an Array frame.
    ///
    /// # Panics
    ///
    /// panics if `self` is not an array
    // 将一个 Integer 帧放入 Array 帧中
    pub(crate) fn push_int(&mut self, value: u64) {
        match self {
            Frame::Array(vec) => {
                vec.push(Frame::Integer(value));
            }
            _ => panic!("not an array frame"),
        }
    }

    /// Checks if an entire message can be decoded from `src`
    // 如果什么都还没读到或者没有读到一个完整的行(一个完整的帧)的话,
    // 返回 Error::Incomplete， 外层 check 如果收到这个错误, 会返回 None
    // 先读取一个字节, 根据这个字节来判断是什么命令, 然后读取一行, 这一行是返回帧的长度,
    // 然后根据这个长度来读取相应的字节数, 最后把字节转换成字符串返回.
    pub fn check(src: &mut Cursor<&[u8]>) -> Result<(), Error> {
        match get_u8(src)? {
            b'+' => {
                // 读取一行, 并跳过这个帧的长度+2个字节, 因为后面还有 \r\n
                get_line(src)?;
                Ok(())
            }
            b'-' => {
                get_line(src)?;
                Ok(())
            }
            // 如果是数字, 那么就读取一行就 OK 了, 数字没有写长度. get_decimal 调用 get_line,
            // 并用 atoi 解析数字, 解析失败直接报错
            b':' => {
                let _ = get_decimal(src)?;
                Ok(())
            }
            // 如果是 $ 开头的话, 那么有可能是 Null, 也有可能是 Bulk
            b'$' => {
                // 如果接下来的是 -， 说明是 Null， 否则是 Bulk
                if b'-' == peek_u8(src)? {
                    // Skip '-1\r\n'
                    // 是 Null, 直接跳过4个字节. 如果长度不够会返回 Error::Incomplete 以继续读
                    skip(src, 4)
                } else {
                    // Read the bulk string
                    // 拿到这个帧的长度
                    let len: usize = get_decimal(src)?.try_into()?;

                    // skip that number of bytes + 2 (\r\n).
                    // 跳过这个帧的长度 + 2 个字节, 因为后面还有 \r\n. 如果长度不够会返回 Error::Incomplete 以继续读
                    skip(src, len + 2)
                }
            }
            // 如果是 Array 的话, 那么先拿到这个 Array 帧的数量, 然后依次解析这些帧
            b'*' => {
                let len = get_decimal(src)?;

                for _ in 0..len {
                    Frame::check(src)?;
                }

                Ok(())
            }
            actual => Err(format!("protocol error; invalid frame type byte `{}`", actual).into()),
        }
    }

    /// The message has already been validated with `check`.
    pub fn parse(src: &mut Cursor<&[u8]>) -> Result<Frame, Error> {
        match get_u8(src)? {
            b'+' => {
                // Read the line and convert it to `Vec<u8>`
                // 拿到一行(一个完整帧)的数据, 不包括 \r\n
                let line = get_line(src)?.to_vec();

                // Convert the line to a String
                // 将这个 Vec<u8> 转换成 String
                let string = String::from_utf8(line)?;
                // 返回这个帧
                Ok(Frame::Simple(string))
            }
            b'-' => {
                // 跟上面一样, 只不过这个帧是一个错误信息
                // Read the line and convert it to `Vec<u8>`
                let line = get_line(src)?.to_vec();

                // Convert the line to a String
                let string = String::from_utf8(line)?;

                Ok(Frame::Error(string))
            }
            b':' => {
                // 返回一个数字. get_decimal 调用 get_line, 并用 atoi 解析数字
                let len = get_decimal(src)?;
                Ok(Frame::Integer(len))
            }
            b'$' => {
                // 如果接下来的是 -, 说明是 Null, 否则是 Bulk
                if b'-' == peek_u8(src)? {
                    let line = get_line(src)?;

                    if line != b"-1" {
                        return Err("protocol error; invalid frame format".into());
                    }

                    Ok(Frame::Null)
                } else {
                    // Read the bulk string
                    // 先读取这个 Bulk 帧的长度(不包括\r\n), 然后再读取相应的字节数,
                    // 然后跳过这么多字节(加上\r\n), 最后返回一个 Bulk 帧
                    let len = get_decimal(src)?.try_into()?; // 这里会跳过相应的字节数
                                                             // 这里加上 2 是因为后面还有 \r\n
                    let n = len + 2;

                    // 如果长度不够, 说明数据不够完整, 返回 Error::Incomplete 以继续从 socket 读取数据
                    if src.remaining() < n {
                        return Err(Error::Incomplete);
                    }

                    // 据从 buffer 中取对应长度的数据, 转换成 Bytes
                    let data = Bytes::copy_from_slice(&src.chunk()[..len]);

                    // skip that number of bytes + 2 (\r\n).
                    // 跳过这个帧的长度+2个字节, 因为后面还有 \r\n. 如果长度不够会返回 Error::Incomplete 以继续读
                    skip(src, n)?;

                    Ok(Frame::Bulk(data))
                }
            }
            // 如果是 Array 帧, 那么需要先读取这个 Array 帧的数量, 然后依次解析这些帧, 最后返回这个完整的 Array 帧
            b'*' => {
                // 拿到这个 Array 的帧的数量
                let len = get_decimal(src)?.try_into()?;
                // 根据这个数量来创建对应 capacity 的数组
                let mut out = Vec::with_capacity(len);

                for _ in 0..len {
                    // 将解析出来的帧放入数组中
                    out.push(Frame::parse(src)?);
                }
                // 返回这个 Array 帧
                Ok(Frame::Array(out))
            }
            _ => unimplemented!(),
        }
    }

    /// Converts the frame to an "unexpected frame" error
    pub(crate) fn to_error(&self) -> crate::Error {
        format!("unexpected frame: {}", self).into()
    }
}

impl PartialEq<&str> for Frame {
    fn eq(&self, other: &&str) -> bool {
        match self {
            Frame::Simple(s) => s.eq(other),
            Frame::Bulk(s) => s.eq(other),
            _ => false,
        }
    }
}

impl fmt::Display for Frame {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        use std::str;

        match self {
            Frame::Simple(response) => response.fmt(fmt),
            Frame::Error(msg) => write!(fmt, "error: {}", msg),
            Frame::Integer(num) => num.fmt(fmt),
            Frame::Bulk(msg) => match str::from_utf8(msg) {
                Ok(string) => string.fmt(fmt),
                Err(_) => write!(fmt, "{:?}", msg),
            },
            Frame::Null => "(nil)".fmt(fmt),
            Frame::Array(parts) => {
                for (i, part) in parts.iter().enumerate() {
                    if i > 0 {
                        // use space as the array element display separator
                        write!(fmt, " ")?;
                    }

                    part.fmt(fmt)?;
                }

                Ok(())
            }
        }
    }
}

fn peek_u8(src: &mut Cursor<&[u8]>) -> Result<u8, Error> {
    if !src.has_remaining() {
        return Err(Error::Incomplete);
    }
    // chunk 返回当前位置至最后一个字节的引用
    // 这里可以安全的取第一个, 因为上面 has_remaining 已经判断过有数据了
    Ok(src.chunk()[0])
}

fn get_u8(src: &mut Cursor<&[u8]>) -> Result<u8, Error> {
    if !src.has_remaining() {
        return Err(Error::Incomplete);
    }
    // 读一个字节, 并返回. 这里里面会调用 advance, 所以 position 也会增加 1
    Ok(src.get_u8())
}

fn skip(src: &mut Cursor<&[u8]>, n: usize) -> Result<(), Error> {
    // 如果长度不够, 说明数据不够完整, 返回 Error::Incomplete
    if src.remaining() < n {
        return Err(Error::Incomplete);
    }

    // 将 pos 移动到 n 位置, 跳过 n 个字节
    // advance 调用的 set_position
    src.advance(n);
    Ok(())
}

/// Read a new-line terminated decimal
fn get_decimal(src: &mut Cursor<&[u8]>) -> Result<u64, Error> {
    use atoi::atoi;

    let line = get_line(src)?;

    atoi::<u64>(line).ok_or_else(|| "protocol error; invalid frame format".into())
}

/// Find a line
// 读一整行, 并跳过(跳过部分包括\r\n)
fn get_line<'a>(src: &mut Cursor<&'a [u8]>) -> Result<&'a [u8], Error> {
    // Scan the bytes directly
    let start = src.position() as usize;
    // Scan to the second to last byte
    // 这里减一是因为下面的 for 循环会取到第 i + 1 位, 索引是不包含最后一位的哈
    let end = src.get_ref().len() - 1;

    for i in start..end {
        if src.get_ref()[i] == b'\r' && src.get_ref()[i + 1] == b'\n' {
            // We found a line, update the position to be *after* the \n
            // 设置 position 到 i + 2, 表示这整个一行(一个完整的帧)已经读取完毕, 包括 \r\n
            src.set_position((i + 2) as u64);

            // Return the line
            // 返回这一行(一个完整的帧)的数据内容, 不包含 \r\n
            return Ok(&src.get_ref()[start..i]);
        }
    }

    Err(Error::Incomplete)
}

impl From<String> for Error {
    fn from(src: String) -> Error {
        Error::Other(src.into())
    }
}

impl From<&str> for Error {
    fn from(src: &str) -> Error {
        src.to_string().into()
    }
}

impl From<FromUtf8Error> for Error {
    fn from(_src: FromUtf8Error) -> Error {
        "protocol error; invalid frame format".into()
    }
}

impl From<TryFromIntError> for Error {
    fn from(_src: TryFromIntError) -> Error {
        "protocol error; invalid frame format".into()
    }
}

impl std::error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::Incomplete => "stream ended early".fmt(fmt),
            Error::Other(err) => err.fmt(fmt),
        }
    }
}
