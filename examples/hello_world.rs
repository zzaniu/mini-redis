//! Hello world server.
//!
//! A simple client that connects to a mini-redis server, sets key "hello" with value "world",
//! and gets it from the server after
//!
//! You can test this out by running:
//!
//!     cargo run --bin mini-redis-server
//!
//! And then in another terminal run:
//!
//!     cargo run --example hello_world

#![warn(rust_2018_idioms)]

use mini_redis::{clients::Client, Result};

#[tokio::main]
pub async fn main() -> Result<()> {
    // Open a connection to the mini-redis address.
    let mut client = Client::connect("127.0.0.1:6379").await?;

    // Set the key "hello" with value "world"
    // 发送 set 命令, 调用 Set::new 生成一个 Set, 在 set_cmd 中将 Set 转成 Frame,
    // (这里是 Frame::array, 里面存放 Frame::Bulk 类型. set, key, value 都是一个 Frame::Bulk.
    // 如果有 px 的话, 还会有 px, expire 这两个 Frame::Bulk.)
    // 然后调用 write_frame 写入 socket, 调用 read_response 读取响应
    // 白话: 将 hello 这个 key 设为 world 这个值, 且无超时限制
    client.set("hello", "world".into()).await?;

    // Get key "hello"
    // 生成一个 Get, 然后转成 Frame::Array, 里面存放两个 Frame::Bulk(get, key)
    // 然后调用 write_frame 写入 socket, 调用 read_response 读取响应
    // 如果是简单的, 返回 Frame::Simple, 如果是批量的, 返回 Frame::Bulk. 如果不存在, 返回 Frame::Null.
    // 其他情况视为错误.
    // read_response 函数会处理 Frame::Error
    // read_response 调用 read_frame 解析帧.
    // 白话: 获取 hello 这个 key 的值, 无超时限制
    let result = client.get("hello").await?;

    println!("got value from the server; success={:?}", result.is_some());

    Ok(())
}
